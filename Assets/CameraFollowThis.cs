﻿using UnityEngine;
using System.Collections;

public class CameraFollowThis : MonoBehaviour {
	// Use this for initialization
	void Start () {
		//Camera.main.transform.parent = transform;
	}
	void Update () {
		// We can't just set the camera's transform parent to the object,
		// because we don't want it to take rotation into account.
		Vector3 p = Camera.main.transform.position;
		p.x = transform.position.x;
		p.y = transform.position.y;
		Camera.main.transform.position = p;
	}

}
