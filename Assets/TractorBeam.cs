﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TractorBeam : MonoBehaviour {

	public int numPoints;
	public float width;
	public float stretchSpeed;
	public float maxStretch;
	
	private LineRenderer lineRenderer;
	private GameObject sphere;
	private GameObject grabberSphere;

	private float[] offsets; // "horizontal" offset for each point
	private float scaleFactor;
	private System.Random rand = new System.Random();
	// Use this for initialization
	void Start () {
		lineRenderer = gameObject.GetComponent<LineRenderer> ();
		sphere = GameObject.Find ("Sphere");
		grabberSphere = GameObject.Find ("Grabber sphere");
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButton (0)) {
			if(!lineRenderer.enabled) {
				// Activate
				offsets = initializeMiddlePoints();
				scaleFactor = 1f;
			}
			grabberSphere.SetActive(true);
			grabberSphere.transform.position = sphere.transform.position;
			lineRenderer.enabled = true;
			Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			List<Vector2> positions = getPositions (sphere.transform.position, mousePosition);

			lineRenderer.SetVertexCount(positions.Count);
			for(int i=0;i<positions.Count;i++) {
				lineRenderer.SetPosition (i, positions[i]);
			}

			scaleFactor += stretchSpeed * (maxStretch - scaleFactor);
		} else {
			lineRenderer.enabled = false;
			grabberSphere.SetActive(false);
		}
	}

	float[] initializeMiddlePoints() {
		float[] ret = new float[numPoints];
		ret [0] = 0;
		ret [numPoints-1] = 0;
		for (int i=1; i<numPoints-1; i++) {
			ret [i] = (float) ((rand.NextDouble() * 2) - 1);
		}
		// Interpolate to smooth
		float[] ret2 = new float[numPoints];
		for (int i=1; i<numPoints-1; i++) {
			ret2[i] = (ret[i-1] + ret[i] + ret[i+1]) / 3f;
		}
		return ret2;
	}

	List<Vector2> getPositions(Vector2 start, Vector2 end) {
		// Perturb offsets, except first and last
		for (int i=1; i<numPoints-1; i++) {
			offsets[i] = offsets[i] + ((((float) rand.NextDouble()) * 2) - 1) / (float) numPoints;
			offsets[i] *= 0.95f;
		}

		// Translate offsets so they render between the click point and the sphere
		List<Vector2> ret = new List<Vector2>();
		for(int i=0;i<numPoints;i++) {
			// when i is 0, x is 0
			// when i is 9, x is 1
			float x = i/(float)(numPoints-1);
			Vector2 toAdd = new Vector2(x, offsets[i]);
			Vector2 endstart = (end - start);

			// scale
			toAdd.x *= scaleFactor;
			if(toAdd.x > 1) {
				continue;
			}
			toAdd.x = toAdd.x * endstart.magnitude;
			toAdd.y *= width;

			// rotate
			toAdd = Quaternion.FromToRotation(Vector2.right, end - start) * toAdd;

			// translate
			toAdd = toAdd + start;
			
			ret.Add(toAdd);
		}
		return ret;
	}
}
