﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

	public float RotateSpeedAlongY;
	public float RotateSpeedAlongZ;
	public float RotateSpeedAlongX;

	static Quaternion staticRotation;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		transform.rotation = staticRotation;

		// Slowly rotate the object around its axis at 1 degree/second * variable.
		transform.Rotate(Vector3.up * Time.deltaTime * RotateSpeedAlongY);
		transform.Rotate(Vector3.forward * Time.deltaTime * RotateSpeedAlongZ);
		transform.Rotate(Vector3.right * Time.deltaTime * RotateSpeedAlongX);

		staticRotation = transform.rotation;
	}
}
