﻿using UnityEngine;
using System.Collections;

public class InitialForce : MonoBehaviour {

	public Vector2 initialForce;
	// Use this for initialization
	void Start () {
		gameObject.rigidbody2D.AddForce (initialForce);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
