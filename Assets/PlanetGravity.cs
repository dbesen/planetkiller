﻿using UnityEngine;
using System.Collections;

public class PlanetGravity : MonoBehaviour {

	public float mass;

	// Use this for initialization
	void Start () {
	}

	void FixedUpdate() {
		// Calculate gravity against all other objects that provide it
		GameObject[] objects = GameObject.FindObjectsOfType<GameObject> ();
		foreach(GameObject ob in objects) {
			PlanetGravity g = ob.GetComponent<PlanetGravity>();
			if(g != null) {
				Vector2 direction = (ob.transform.position - gameObject.transform.position).normalized * mass * g.mass;
				float distance = Vector2.Distance (ob.transform.position, gameObject.transform.position);
				if(distance > 0) {
					direction /= distance * distance;
					gameObject.rigidbody2D.AddForce (direction);
				}
			}
		}
	}


}
