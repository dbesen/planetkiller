﻿using UnityEngine;
using System.Collections;

public class ClickGravity : MonoBehaviour {

	public float gravityScale;
	public bool normalizeBodyVelocity;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		// TODO: Should this be in Update, FixedUpdate, or some combination thereof?
		if (Input.GetMouseButton (0)) {
			Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector2 direction = (mousePosition - (Vector2) gameObject.transform.position).normalized * gravityScale;
			// We want to turn the vector towards 'direction' without changing its magnitude.
			// Add them together, then normalize magnitude.

			// There's probably some way to simplify this math.
			float magnitudeBefore = rigidbody2D.velocity.magnitude;
			rigidbody2D.velocity += direction;
			float magnitudeAfter = rigidbody2D.velocity.magnitude;

			if(normalizeBodyVelocity) {
				float changeInMagnitude = magnitudeAfter / magnitudeBefore;
				rigidbody2D.velocity /= changeInMagnitude;
			}
		}
	
	}
}
