﻿using UnityEngine;
using System.Collections;

// TODO: this should be 3 scripts:
// - Cause win if we hit the planet (our asteroid or red asteroid)
// - Cause loss if we exit the screen (our asteroid only)
// - Cause loss if we hit anything that's not the planet (our asteroid and red asteroid)
public class CausesWinOrLoss : MonoBehaviour {

	public bool winIfHitsPlanet;
	public bool loseIfExitsScreen;
	public bool loseIfHitsOther;
	public bool winIfGoesPastX;
	public float goesPastXValue;

	void OnCollisionEnter2D(Collision2D collision) {
		// What did we collide with?
		if (collision.gameObject.name == "Planet") {
			if(winIfHitsPlanet)
				winLevel();
		} else {
			if(loseIfHitsOther)
				loseLevel();
		}
	}
	
	void Update () {
		// If we leave the screen, we lose the level
		if (loseIfExitsScreen) {
			Vector2 viewportPosition = Camera.main.WorldToViewportPoint (transform.position);
			if (viewportPosition.x < 0 || viewportPosition.y < 0 || viewportPosition.x > 1 || viewportPosition.y > 1) {
				loseLevel ();
			}
		}
		if (winIfGoesPastX) {
			if (transform.position.x > goesPastXValue) {
				winLevel ();
			}
		}
	}	

	void winLevel() {
		if (Application.loadedLevel == Application.levelCount - 1) {
			// We finished all the levels; for now, just start over
			Application.LoadLevel (0);
		} else {
			Application.LoadLevel (Application.loadedLevel + 1);
		}
	}
	
	void loseLevel() {
		Application.LoadLevel (Application.loadedLevelName);
	}
}
